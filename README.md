<div style="text-align:center;">
<a href="http://bukly.herokuapp.com"><img src="http://res.cloudinary.com/habu-kagumba/image/upload/v1470398003/rapid_runty_dpkdaq.svg" alt="Rapid Runty" width="200"></a>
</div>


# Rapid Runty

 [![Gem Version](https://badge.fury.io/rb/rapid_runty.svg)](https://badge.fury.io/rb/rapid_runty) [![Dependency Status](https://gemnasium.com/badges/bitbucket.org/habu_kagumba/rapid_runty.svg)](https://gemnasium.com/bitbucket.org/habu_kagumba/rapid_runty) [ ![Codeship Status for habu_kagumba/rapid_runty](https://codeship.com/projects/f506dfd0-3e2e-0134-298e-365d6082cf0e/status?branch=master) ](https://codeship.com/projects/167314) [![Coverage Status](https://coveralls.io/repos/bitbucket/habu_kagumba/rapid_runty/badge.svg?branch=master)](https://coveralls.io/bitbucket/habu_kagumba/rapid_runty?branch=master) [![codebeat badge](https://codebeat.co/badges/1c48e76c-c7c7-484a-a5d5-7c397b4fb429)](https://codebeat.co/projects/bitbucket-org-habu_kagumba-rapid_runty)

**Rapid Runty** is a minimal web framework to get your web project up and running in seconds.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'rapid_runty'
```

And then execute:

```bash
$ bundle
```

Or install it yourself as:

```bash
$ gem install rapid_runty
```

## Usage

TODO: Write usage instructions here

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/andela-hkagumba/rapid_runty. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

